import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DeleteNumbersTest {

    private DeleteNumbers deleteNumbers = new DeleteNumbers();

    @Test
    public void imp1(){
        Throwable exception = assertThrows(NullPointerException.class, () -> DeleteNumbers.deleteNumbers("TestFiles/"));
        assertEquals(null, exception.getMessage(), "Teste para validar que não existem ficheiros de texto no diretorio enviado por parametro");
        }

@Test
public void imp2(){
        Throwable exception = assertThrows(NullPointerException.class, () -> DeleteNumbers.deleteNumbers("TeasdasdstFiles"));
        assertEquals(null, exception.getMessage(), "Teste para validar que não existe o diretorio enviado por parametro");
        }
        }