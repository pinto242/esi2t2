import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DeletePchars {
    public static void main(String[] args) {
        deletePchars("TextFiles/");
    }


    public static void deletePchars(String directory) {

        List<String> lines = new ArrayList<String>();
        String line;

        List<String> textFiles = new ArrayList<String>();
        File dir = new File(directory);
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith((".txt"))) {
                textFiles.add(file.getName());
            }
        }

        for (int i = 0; i < textFiles.size(); i++) {

            try {
                File f1 = new File(directory + textFiles.get(i));

                FileReader fr = new FileReader(f1);
                BufferedReader br = new BufferedReader(fr);
                while ((line = br.readLine()) != null) {

                    line = line.replaceAll("[^a-z A-Z 0-9]", "");
                    lines.add(line);
                    lines.add("\n");
                }
                fr.close();
                br.close();

                Writer fw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(directory + textFiles.get(i)), "UTF-8"));
                BufferedWriter out = new BufferedWriter(fw);
                for (String s : lines)
                    out.write(s);
                out.flush();
                out.close();
                lines.clear();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}


